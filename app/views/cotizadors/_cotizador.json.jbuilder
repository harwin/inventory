json.extract! cotizador, :id, :nombre_cliente, :servisios, :comentario, :email, :telefono, :direccion, :created_at, :updated_at
json.url cotizador_url(cotizador, format: :json)
