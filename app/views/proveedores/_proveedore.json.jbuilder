json.extract! proveedore, :id, :nombre_empresa, :telefono_empresa, :correo_empresa, :nombre_contacto, :telefono_contacto, :correo_contacto, :created_at, :updated_at
json.url proveedore_url(proveedore, format: :json)
