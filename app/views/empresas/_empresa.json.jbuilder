json.extract! empresa, :id, :name, :mail, :typo, :razon_social, :telefono, :direccion, :created_at, :updated_at
json.url empresa_url(empresa, format: :json)
