json.extract! servicios_click, :id, :nombre, :descripcion, :url_imagen, :created_at, :updated_at
json.url servicios_click_url(servicios_click, format: :json)
