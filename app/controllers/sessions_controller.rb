class SessionsController < ApplicationController
  def new
    @user = User.new
  end
  #Login metodo que cree la session.
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      # Log the user in and redirect to the user's show page.
      flash[:success] = "Welcome to Counct!"
      log_in user
      redirect_to user
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end
#cierra la session.
  def destroy
    log_out
    redirect_to root_url
    # redirect_to logint_url
  end
end
