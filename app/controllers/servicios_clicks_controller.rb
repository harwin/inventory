class ServiciosClicksController < ApplicationController
  before_action :set_servicios_click, only: [:show, :edit, :update, :destroy]

  # GET /servicios_clicks
  # GET /servicios_clicks.json
  def index
    @servicios_clicks = ServiciosClick.all
  end

  # GET /servicios_clicks/1
  # GET /servicios_clicks/1.json
  def show
  end

  # GET /servicios_clicks/new
  def new
    @servicios_click = ServiciosClick.new
  end

  # GET /servicios_clicks/1/edit
  def edit
  end

  # POST /servicios_clicks
  # POST /servicios_clicks.json
  def create
    @servicios_click = ServiciosClick.new(servicios_click_params)

    respond_to do |format|
      if @servicios_click.save
        format.html { redirect_to @servicios_click, notice: 'Servicios click was successfully created.' }
        format.json { render :show, status: :created, location: @servicios_click }
      else
        format.html { render :new }
        format.json { render json: @servicios_click.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /servicios_clicks/1
  # PATCH/PUT /servicios_clicks/1.json
  def update
    respond_to do |format|
      if @servicios_click.update(servicios_click_params)
        format.html { redirect_to @servicios_click, notice: 'Servicios click was successfully updated.' }
        format.json { render :show, status: :ok, location: @servicios_click }
      else
        format.html { render :edit }
        format.json { render json: @servicios_click.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /servicios_clicks/1
  # DELETE /servicios_clicks/1.json
  def destroy
    @servicios_click.destroy
    respond_to do |format|
      format.html { redirect_to servicios_clicks_url, notice: 'Servicios click was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_servicios_click
      @servicios_click = ServiciosClick.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def servicios_click_params
      params.require(:servicios_click).permit(:nombre, :descripcion, :url_imagen)
    end
end
