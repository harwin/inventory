class CotizadorsController < ApplicationController
  before_action :set_cotizador, only: [:show, :edit, :update, :destroy]

  # GET /cotizadors
  # GET /cotizadors.json
  def index
       if logged_in?
    @cotizadors = Cotizador.all
       else
         redirect_to login_url
       end
  end

  # GET /cotizadors/1
  # GET /cotizadors/1.json
  def show
  end

  # GET /cotizadors/new
  def new
    @cotizador = Cotizador.new
  end

  # GET /cotizadors/1/edit
  def edit
  end

  # POST /cotizadors
  # POST /cotizadors.json
  def create
    @cotizador = Cotizador.new(cotizador_params)

    respond_to do |format|
      if @cotizador.save
        format.html { redirect_to @cotizador, notice: 'Cotizador was successfully created.' }
        format.json { render :show, status: :created, location: @cotizador }
      else
        format.html { render :new }
        format.json { render json: @cotizador.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cotizadors/1
  # PATCH/PUT /cotizadors/1.json
  def update
    respond_to do |format|
      if @cotizador.update(cotizador_params)
        format.html { redirect_to @cotizador, notice: 'Cotizador was successfully updated.' }
        format.json { render :show, status: :ok, location: @cotizador }
      else
        format.html { render :edit }
        format.json { render json: @cotizador.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cotizadors/1
  # DELETE /cotizadors/1.json
  def destroy
    @cotizador.destroy
    respond_to do |format|
      format.html { redirect_to cotizadors_url, notice: 'Cotizador was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cotizador
      @cotizador = Cotizador.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cotizador_params
      params.require(:cotizador).permit(:nombre_cliente, :servisios, :comentario, :email, :telefono, :direccion)
    end
end
