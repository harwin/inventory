class Cotizador < ApplicationRecord
    validates :nombre_cliente, presence: true, length: { maximum: 150 }
    validates :servisios, presence: true, length: { maximum: 11}
    validates :email, presence: true, length: { maximum: 120}
    validates :telefono, presence: true, length: { maximum: 15}
    
     
end
