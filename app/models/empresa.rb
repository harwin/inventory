class Empresa < ApplicationRecord
    validates :name, presence: true, length: { maximum: 20 }
    VALID_MAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :mail, presence:   true,
                    format:     { with: VALID_MAIL_REGEX },
                    uniqueness: { case_sensitive: false }

validates :razon_social, presence: true, length: { maximum: 120 }
validates :telefono, presence: true, length: { maximum: 15 }
validates :direccion, presence: true, length: { maximum: 10 }


end
