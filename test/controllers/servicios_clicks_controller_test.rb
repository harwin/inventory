require 'test_helper'

class ServiciosClicksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @servicios_click = servicios_clicks(:one)
  end

  test "should get index" do
    get servicios_clicks_url
    assert_response :success
  end

  test "should get new" do
    get new_servicios_click_url
    assert_response :success
  end

  test "should create servicios_click" do
    assert_difference('ServiciosClick.count') do
      post servicios_clicks_url, params: { servicios_click: { descripcion: @servicios_click.descripcion, nombre: @servicios_click.nombre, url_imagen: @servicios_click.url_imagen } }
    end

    assert_redirected_to servicios_click_url(ServiciosClick.last)
  end

  test "should show servicios_click" do
    get servicios_click_url(@servicios_click)
    assert_response :success
  end

  test "should get edit" do
    get edit_servicios_click_url(@servicios_click)
    assert_response :success
  end

  test "should update servicios_click" do
    patch servicios_click_url(@servicios_click), params: { servicios_click: { descripcion: @servicios_click.descripcion, nombre: @servicios_click.nombre, url_imagen: @servicios_click.url_imagen } }
    assert_redirected_to servicios_click_url(@servicios_click)
  end

  test "should destroy servicios_click" do
    assert_difference('ServiciosClick.count', -1) do
      delete servicios_click_url(@servicios_click)
    end

    assert_redirected_to servicios_clicks_url
  end
end
