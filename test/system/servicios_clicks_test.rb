require "application_system_test_case"

class ServiciosClicksTest < ApplicationSystemTestCase
  setup do
    @servicios_click = servicios_clicks(:one)
  end

  test "visiting the index" do
    visit servicios_clicks_url
    assert_selector "h1", text: "Servicios Clicks"
  end

  test "creating a Servicios click" do
    visit servicios_clicks_url
    click_on "New Servicios Click"

    fill_in "Descripcion", with: @servicios_click.descripcion
    fill_in "Nombre", with: @servicios_click.nombre
    fill_in "Url imagen", with: @servicios_click.url_imagen
    click_on "Create Servicios click"

    assert_text "Servicios click was successfully created"
    click_on "Back"
  end

  test "updating a Servicios click" do
    visit servicios_clicks_url
    click_on "Edit", match: :first

    fill_in "Descripcion", with: @servicios_click.descripcion
    fill_in "Nombre", with: @servicios_click.nombre
    fill_in "Url imagen", with: @servicios_click.url_imagen
    click_on "Update Servicios click"

    assert_text "Servicios click was successfully updated"
    click_on "Back"
  end

  test "destroying a Servicios click" do
    visit servicios_clicks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Servicios click was successfully destroyed"
  end
end
