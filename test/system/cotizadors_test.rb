require "application_system_test_case"

class CotizadorsTest < ApplicationSystemTestCase
  setup do
    @cotizador = cotizadors(:one)
  end

  test "visiting the index" do
    visit cotizadors_url
    assert_selector "h1", text: "Cotizadors"
  end

  test "creating a Cotizador" do
    visit cotizadors_url
    click_on "New Cotizador"

    fill_in "Comentario", with: @cotizador.comentario
    fill_in "Direccion", with: @cotizador.direccion
    fill_in "Email", with: @cotizador.email
    fill_in "Nombre cliente", with: @cotizador.nombre_cliente
    fill_in "Servisios", with: @cotizador.servisios
    fill_in "Telefono", with: @cotizador.telefono
    click_on "Create Cotizador"

    assert_text "Cotizador was successfully created"
    click_on "Back"
  end

  test "updating a Cotizador" do
    visit cotizadors_url
    click_on "Edit", match: :first

    fill_in "Comentario", with: @cotizador.comentario
    fill_in "Direccion", with: @cotizador.direccion
    fill_in "Email", with: @cotizador.email
    fill_in "Nombre cliente", with: @cotizador.nombre_cliente
    fill_in "Servisios", with: @cotizador.servisios
    fill_in "Telefono", with: @cotizador.telefono
    click_on "Update Cotizador"

    assert_text "Cotizador was successfully updated"
    click_on "Back"
  end

  test "destroying a Cotizador" do
    visit cotizadors_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cotizador was successfully destroyed"
  end
end
