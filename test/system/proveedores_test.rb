require "application_system_test_case"

class ProveedoresTest < ApplicationSystemTestCase
  setup do
    @proveedore = proveedores(:one)
  end

  test "visiting the index" do
    visit proveedores_url
    assert_selector "h1", text: "Proveedores"
  end

  test "creating a Proveedore" do
    visit proveedores_url
    click_on "New Proveedore"

    fill_in "Correo contacto", with: @proveedore.correo_contacto
    fill_in "Correo empresa", with: @proveedore.correo_empresa
    fill_in "Nombre contacto", with: @proveedore.nombre_contacto
    fill_in "Nombre empresa", with: @proveedore.nombre_empresa
    fill_in "Telefono contacto", with: @proveedore.telefono_contacto
    fill_in "Telefono empresa", with: @proveedore.telefono_empresa
    click_on "Create Proveedore"

    assert_text "Proveedore was successfully created"
    click_on "Back"
  end

  test "updating a Proveedore" do
    visit proveedores_url
    click_on "Edit", match: :first

    fill_in "Correo contacto", with: @proveedore.correo_contacto
    fill_in "Correo empresa", with: @proveedore.correo_empresa
    fill_in "Nombre contacto", with: @proveedore.nombre_contacto
    fill_in "Nombre empresa", with: @proveedore.nombre_empresa
    fill_in "Telefono contacto", with: @proveedore.telefono_contacto
    fill_in "Telefono empresa", with: @proveedore.telefono_empresa
    click_on "Update Proveedore"

    assert_text "Proveedore was successfully updated"
    click_on "Back"
  end

  test "destroying a Proveedore" do
    visit proveedores_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Proveedore was successfully destroyed"
  end
end
