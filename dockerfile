#Dockerfile
FROM ruby:2.6.0
RUN  apt-get update -qq &&  apt-get install -y nodejs postgresql-client
RUN gem install bundler -v '2.1.4' 
RUN mkdir /inventory
WORKDIR /inventory
COPY Gemfile /inventory/Gemfile
COPY Gemfile.lock /inventory/Gemfile.lock
RUN curl https://deb.nodesource.com/setup_12.x | bash
RUN curl https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y nodejs yarn postgresql-client
RUN bundle install 
RUN yarn install
RUN yarn install --check-files
COPY . /inventory
CMD ["rails","server","-b","0.0.0.0"]