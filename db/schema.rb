# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_29_043728) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cotizadors", force: :cascade do |t|
    t.string "nombre_cliente"
    t.string "servisios"
    t.text "comentario"
    t.string "email"
    t.string "telefono"
    t.string "direccion"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "empresas", force: :cascade do |t|
    t.string "name"
    t.string "mail"
    t.integer "typo"
    t.string "razon_social"
    t.integer "telefono"
    t.string "direccion"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["typo"], name: "index_empresas_on_typo"
  end

  create_table "menus", force: :cascade do |t|
    t.string "name"
    t.string "options"
    t.integer "id_users"
    t.string "relacion"
    t.string "Users"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["id_users"], name: "index_menus_on_id_users"
  end

  create_table "proveedores", force: :cascade do |t|
    t.string "nombre_empresa"
    t.string "telefono_empresa"
    t.string "correo_empresa"
    t.string "nombre_contacto"
    t.string "telefono_contacto"
    t.string "correo_contacto"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "servicios", force: :cascade do |t|
    t.string "name"
    t.string "descripcion"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "servicios_clicks", force: :cascade do |t|
    t.string "nombre"
    t.string "descripcion"
    t.string "url_imagen"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

# Could not dump table "users" because of following StandardError
#   Unknown type 'menus' for column 'id_users'

  create_table "users_empresas", force: :cascade do |t|
    t.bigint "users_id", null: false
    t.bigint "empresas_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["empresas_id"], name: "index_users_empresas_on_empresas_id"
    t.index ["users_id"], name: "index_users_empresas_on_users_id"
  end

  create_table "users_menus", force: :cascade do |t|
    t.bigint "users_id", null: false
    t.bigint "menus_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["menus_id"], name: "index_users_menus_on_menus_id"
    t.index ["users_id"], name: "index_users_menus_on_users_id"
  end

  add_foreign_key "users_empresas", "empresas", column: "empresas_id"
  add_foreign_key "users_empresas", "users", column: "users_id"
  add_foreign_key "users_menus", "menus", column: "menus_id"
  add_foreign_key "users_menus", "users", column: "users_id"
end
