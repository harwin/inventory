class CreateProveedores < ActiveRecord::Migration[6.0]
  def change
    create_table :proveedores do |t|
      t.string :nombre_empresa
      t.string :telefono_empresa
      t.string :correo_empresa
      t.string :nombre_contacto
      t.string :telefono_contacto
      t.string :correo_contacto

      t.timestamps
    end
  end
end
