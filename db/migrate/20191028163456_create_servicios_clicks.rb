class CreateServiciosClicks < ActiveRecord::Migration[6.0]
  def change
    create_table :servicios_clicks do |t|
      t.string :nombre
      t.string :descripcion
      t.string :url_imagen

      t.timestamps
    end
  end
end
