class CreateUsersMenus < ActiveRecord::Migration[6.0]
  def change
    create_table :users_menus do |t|
      t.references :users, null: false, foreign_key: true
      t.references :menus, null: false, foreign_key: true
      t.timestamps
    end
    
  end
end
