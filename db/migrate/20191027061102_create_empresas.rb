class CreateEmpresas < ActiveRecord::Migration[6.0]
  def change
    create_table :empresas do |t|
      t.string :name
      t.string :mail
      t.integer :typo
      t.string :razon_social
      t.integer :telefono
      t.string :direccion

      t.timestamps
    end
    add_index :empresas, :typo
  end
end
