class CreateMenus < ActiveRecord::Migration[6.0]
  def change
    create_table :menus do |t|
      t.string :name
      t.string :options
      t.integer :id_users
      t.string :relacion
      t.string :Users

      t.timestamps
    end
    add_index :menus, :id_users
  end
end
