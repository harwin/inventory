class CreateCotizadors < ActiveRecord::Migration[6.0]
  def change
    create_table :cotizadors do |t|
      t.string :nombre_cliente
      t.string :servisios
      t.text :comentario
      t.string :email
      t.string :telefono
      t.string :direccion

      t.timestamps
    end
  end
end
