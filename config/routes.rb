Rails.application.routes.draw do
  root 'welcome#index'
  resources :proveedores
  resources :servicios_clicks
  resources :cotizadors
  resources :empresas
  resources :menus

  get 'statick_pages/home'
  get 'statick_pages/help'
  get 'statick_pages/conctact'
 # root  'sessions#new'
  get    'help'    => 'static_pages#help'
  get    'about'   => 'static_pages#about'
  get    'contact' => 'static_pages#contact'
  get    'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  get 'logout'  => 'sessions#destroy'
  resources :users
end
